use core::hash::Hash;
use std::collections::HashMap;

struct Memoise<A, B, F> {
    map: HashMap<A, B>,
    fun: F,
}

impl<A, B, F> Memoise<A, B, F>
where
    F: FnMut(&A) -> B,
    A: Eq + Hash,
    B: Clone,
{
    fn new(fun: F) -> Self {
        Self {
            map: HashMap::new(),
            fun,
        }
    }

    fn call(&mut self, par: A) -> B {
        self.map
            .entry(par)
            .or_insert_with_key(|key| (self.fun)(key))
            .clone()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::rc::Rc;
    use std::cell::RefCell;

    fn load(x: &u8) -> u8 {
        println!("Calculating manually: {x}");
        *x
    }

    #[test]
    fn test_1_memoise() {
        let mut load = Memoise::new(load);
        assert_eq!(load.call(8), 8);
        assert_eq!(load.call(7), 7);
        assert_eq!(load.call(8), 8);
    }

    #[test]
    fn test_2_closure() {
        let f = |x: &u8| -> u8 {*x};

        let mut load = Memoise::new(f);

        assert_eq!(load.call(8), 8);
        assert_eq!(load.call(7), 7);
        assert_eq!(load.call(8), 8);
    }

    /// Test that the memoised function is only called once for each input value.
    /// This is achieved by having the test function modify some state when called and
    /// then checking that the state is not changed when calling it on the same value again.
    #[test]
    fn test_3_closure_with_state() {
        let state = Rc::new(RefCell::new(1));
        let track_state = Rc::clone(&state);
        let f = |x: &u8| -> u8 {*state.borrow_mut() += 1; *x};

        let mut load = Memoise::new(f);

        assert_eq!(*track_state.borrow(), 1);
        assert_eq!(load.call(8), 8);
        assert_eq!(*track_state.borrow(), 2);
        assert_eq!(load.call(7), 7);
        assert_eq!(*track_state.borrow(), 3);
        assert_eq!(load.call(8), 8);
        assert_eq!(*track_state.borrow(), 3);
    }
}
